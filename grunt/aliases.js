module.exports = function (grunt, options) {
    var reduce = require('lodash/reduce');
    var size = require('lodash/size');
    var fs = require('fs-extra');
    var path = require('path');
    var slash = require('slash');
    var php = require('php-proxy-middleware');

    // helper
    function getMiddleware(target) {
        if (target === 'dist') {
            process.env['SYMFONY_ENV'] = 'prod';
            process.env['SYMFONY_DEBUG'] = 0;
        } else {
            process.env['SYMFONY_ENV'] = 'node';
            process.env['SYMFONY_DEBUG'] = 1;
        }
        return php({
            address: '127.0.0.1', // which interface to bind to
            ini: {max_execution_time: 60, variables_order: 'EGPCS'},
            root: options.paths.dist,
            router: path.join(options.paths.dist, 'app.php')
        });
    }

    return {
        default: [
            'availabletasks'
        ],
        css: function (target) {
            grunt.task.run([
                'clean:css',
                'less',
                'autoprefixer'
            ]);
            if (target === 'dist') {
                
                grunt.task.run(['twigRender', 'critical', 'uncss', 'cssmin']);
            } else if (target === 'assets') {
                grunt.task.run(['copy:assets-css']);
            }
        },
        js: function () {
            grunt.task.run([
                'clean:js',
                'webpack'
            ]);
        },
        img: function (target) {
            grunt.task.run(['clean:img', 'svgstore', 'svgmin:icons']);

            if (target === 'dist') {
                grunt.task.run([
                    'imagemin',
                    'svgmin:dist'
                ]);
            } else {
                grunt.task.run(['copy:assets-img']);
            }
        },
        rev: [
            'filerev',
            'revdump',
            'usemin'
        ],
        'generate-service-worker': [
            'copy:sw-scripts',
            'sw-precache:dist',
            // fallback for browsers not supporting service workers
            'appcache'
        ],
        assets: [
            'js:assets',
            'css:assets',
            'img:assets',
            'copy:sw-scripts',
            'rev',
            'copy:dist',
            'clean:tmp',
            'generate-service-worker'
        ],
        build: [
            'test',
            'js:dist',
            'css:dist',
            'img:dist',
            'copy:sw-scripts',
            'rev',
            'copy:dist',
            'clean:tmp',
            'generate-service-worker'
        ],
        test: [
            'eslint',
            'karma',
            'phpunit'
        ],
        revdump: function(){
            var file = 'app/config/rev-manifest.json';
            fs.outputJsonSync(file, reduce(grunt.filerev.summary, function(acc,val,key){
                acc[slash(key.replace('web/',''))] = slash(val.replace('web/',''));
                return acc;
            },{}));
        },
        serve: function(target) {
            // clean tmp
            grunt.task.run(['clean:tmp']);

            if (target === 'dist') {
                grunt.task.run(['exec:sfclprod','build']);
            } else {
                target = 'dev';
                grunt.task.run(['svgstore', 'svgmin:icons', 'css:serve']);
            }

            // start php middleware
            grunt.bsMiddleware = getMiddleware(target);

            grunt.task.run([
                'browserSync:'+ target, // Using the php middleware
                'watch'                 // Any other watch tasks you want to run
            ]);
        }
    };
};
