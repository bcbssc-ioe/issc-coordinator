'use strict';
module.exports = {
    app: {
        options: {
            configuration: 'phpunit.xml.dist',
            bin: 'vendor/bin/phpunit',
            followOutput: true,
            color: true
        }
    }
};
