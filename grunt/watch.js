'use strict';
module.exports = {
    styles: {
        files: ['<%= paths.app %>/styles/{,*/}*.less'],
        tasks: ['less','autoprefixer']
    },
    scripts: {
        files: ['<%= paths.app %>/scripts/**/*.js'],
        tasks: ['eslint']
    },
    icons: {
        files: ['<%= paths.app %>/img/svg-icons/*.svg'],
        tasks: ['svgstore','svgmin:icons']
    }
};
